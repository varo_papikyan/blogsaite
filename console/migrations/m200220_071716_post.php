<?php

use yii\db\Migration;

/**
 * Class m200220_071716_post
 */
class m200220_071716_post extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200220_071716_post cannot be reverted.\n";

        return false;
    }

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('post', [
            'category_id' => $this->primaryKey(),
            'title' => $this->string(),
            'expert' => $this->string(300),
            'content' => $this->text(),
            'img'=>$this->string(300),
            'created_at' => $this->string(300),
            'keywords' => $this->string(300),
            'description' => $this->text()
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('post');
    }
}
