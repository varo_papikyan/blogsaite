<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post".
 *
 * @property int $id
 * @property int|null $category_id
 * @property string|null $title
 * @property string|null $expert
 * @property string|null $content
 * @property string|null $img
 * @property string|null $created_at
 * @property string|null $keywords
 * @property string|null $description
 */
class post extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post';
    }
    public function getCategory(){
        return $this->hasOne(Category::class,['id'=>'category_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id'], 'integer'],
            [['content', 'description'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['expert', 'img', 'created_at', 'keywords'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'title' => 'Title',
            'expert' => 'Expert',
            'content' => 'Content',
            'img' => 'Img',
            'created_at' => 'Created At',
            'keywords' => 'Keywords',
            'description' => 'Description',
        ];
    }
}
