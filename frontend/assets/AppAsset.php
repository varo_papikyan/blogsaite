<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [

        "https://fonts.googleapis.com/icon?family=Material+Icons",
        "https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800,900",
        "https://fonts.googleapis.com/css?family=Roboto:400,500,700,900",
        "css/bootstrap.css",
        "css/font-awesome.min.css",
        "css/owl.carousel.min.css",
        "css/owl.theme.default.min.css",
        "style.css",
        "responsive.css"
    ];
    public $js = [
        "js/bootstrap.min.js",
        "js/owl.carousel.min.js",
        "js/active.js"
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
